#include <stdio.h>

/* copy input to output; 2nd version */
int main(int argc, char *argv[]) {
  int c, prev;

  while((c = getchar()) != EOF) {

    if(c == ' ' && prev == ' ') {
      continue;
    }

    putchar(c);
    prev = c;
  }
}

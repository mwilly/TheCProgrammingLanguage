#include <stdio.h>

/* Print Fahrenheit-Celsius table
 * for fahr = 0, 20, ..., 300 */
int main(int argc, char *argv) {
  printf("Fahrenheit Celsius\n");

  float celsius;

  for(float fahr = 300; fahr >= 0; fahr -= 20) {
    celsius = (5.0 / 9.0) * (fahr - 32);
    printf("%6.0f %9.1f\n", fahr, celsius);
  }
}

#include <stdio.h>

#define LOWER 0
#define UPPER 300
#define STEP  20

/* Print Fahrenheit-Celsius table
 * for fahr = 0, 20, ..., 300 */
int main(int argc, char *argv) {
  printf("Fahrenheit Celsius\n");

  for(float fahr = LOWER; fahr <= UPPER; fahr += STEP) {
    printf("%6.0f %9.1f\n", fahr, (5.0 / 9.0) * (fahr - 32.0));
  }
}

#include <stdio.h>

int main(int argc, char *argv[]) {
  long nc = 0;

  while(getchar() != EOF) {
    nc++;
  }
  printf("\nNumber of characters: %ld\n", nc);
}
